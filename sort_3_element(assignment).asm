
org 100h

.data
a db ?
b db ?
c db ?

.code
mov ax,@data
mov ds,ax

mov ah,1
int 21h
mov a,al

mov ah,1
int 21h
mov b,al

mov ah,1
int 21h
mov c,al

mov dl,0dh
mov ah,2
int 21h

mov dl,0ah
mov ah,2
int 21h

mov al,a
mov bl,b
mov cl,c

cmp al,bl

jg jmpifagb

jl jmpifalb



jmpifagb: cmp bl,cl
          jg jmpifagbgc
          jl jmpifagblc
          
jmpifalb: cmp bl,cl
          jl jmpifalblc
          jg jmpifalbgc
          
jmpifagblc: cmp al,cl
            jl jmpifcgagb
            jg jmpifagcgb
            
jmpifalbgc: cmp al,cl
            jl jmpifbgcga
            jg jmpifbgagc
          
         
jmpifagbgc: mov dl,c
            mov ah,2
            int 21h
         
            mov dl,b
            mov ah,2
            int 21h
         
            mov dl,a
            mov ah,2
            int 21h
            jmp end_if
          
          
jmpifalblc: mov dl,a
            mov ah,2
            int 21h
         
            mov dl,b
            mov ah,2
            int 21h
         
            mov dl,c
            mov ah,2
            int 21h
            jmp end_if
            
            
jmpifcgagb: mov dl,b
            mov ah,2
            int 21h
         
            mov dl,a
            mov ah,2
            int 21h
         
            mov dl,c
            mov ah,2
            int 21h
            jmp end_if          

jmpifagcgb: mov dl,b
            mov ah,2
            int 21h
         
            mov dl,c
            mov ah,2
            int 21h
         
            mov dl,a
            mov ah,2
            int 21h
            jmp end_if

jmpifbgcga: mov dl,a
            mov ah,2
            int 21h
         
            mov dl,c
            mov ah,2
            int 21h
         
            mov dl,b
            mov ah,2
            int 21h
            jmp end_if

jmpifbgagc: mov dl,c
            mov ah,2
            int 21h
         
            mov dl,a
            mov ah,2
            int 21h
         
            mov dl,b
            mov ah,2
            int 21h
            jmp end_if


end_if:
          

ret




