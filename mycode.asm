
; You may customize this and other start-up templates; 
; The location of this template is c:\emu8086\inc\0_com_template.txt

org 100h

.data
msg db "Hello World $"
x db "Hello World $"
y db "Have a nice day $"
.code

mov ax, @data
mov ds,ax

lea dx,msg
mov ah,9
int 21h

    mov dl,0ah
    mov ah,2
    int 21h
    
    mov dl,0dh
    mov ah,2
    int 21h
    

lea dx,x
mov ah,9
int 21h
ret




