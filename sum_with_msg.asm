
; You may customize this and other start-up templates; 
; The location of this template is c:\emu8086\inc\0_com_template.txt

org 100h

.data
msg db "the sum of "
x db ?," and "
y db ?," is "
z db ?,"$"

.code
mov ax,@data
mov ds,ax

mov dl,"?"
mov ah,2
int 21h

mov ah,1
int 21h
mov x,al

mov ah,1
int 21h
mov y,al 

add al,x
sub al,30h
mov z,al

   
mov dl,0Ah
mov ah,2
int 21h

mov dl,0Dh
mov ah,2
int 21h

lea dx,msg
mov ah,9
int 21h


ret




