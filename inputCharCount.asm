
; You may customize this and other start-up templates; 
; The location of this template is c:\emu8086\inc\0_com_template.txt

org 100h

mov dx,0dh ;dx counts characters
mov ah,1
int 21h

while_: cmp al,0dh
        je end_while
        
        
inc dx ; not CR , so 
mov ah,1 ;  increment count of DX
int 21h

jmp while_ ; loop back

end_while:

ret




