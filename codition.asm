
; You may customize this and other start-up templates; 
; The location of this template is c:\emu8086\inc\0_com_template.txt

org 100h

.data
msg db "Smaller variable of those variable : $"
msg2 db "Enter first variable to compare : $"
msg3 db "Enter second variable to compare : $"
x db ?
y db ?

.code
mov ax,@data
mov ds,ax

lea dx,msg2
mov ah,9
int 21h

mov ah,1
int 21h
mov x,al

mov dl,0ah
mov ah,2
int 21h
        
mov dl,0dh
mov ah,2
int 21h

lea dx,msg3
mov ah,9
int 21h

mov ah,1
int 21h
mov y,al

mov dl,0ah
mov ah,2
int 21h
        
mov dl,0dh
mov ah,2
int 21h

lea dx,msg
mov ah,9
int 21h

mov al,x

mov bl,y

cmp al,bl

jl level1

jg level2

level1: 
        mov dl,x
        mov ah,2
        int 21h
        jmp end_if

level2:
        mov dl,y
        mov ah,2
        int 21h

end_if:


ret