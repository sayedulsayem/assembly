
; You may customize this and other start-up templates; 
; The location of this template is c:\emu8086\inc\0_com_template.txt

org 100h

.data
msg6 db "***********",0ah,0dh,"$"

msg3 db "****",
x db ?,
y db ?,
z db ?,"****",0ah,0dh,"$"

 
.code
mov ax, @data
mov ds,ax
          
mov dl,"?"
mov ah,2
int 21h

mov ah,1
int 21h
mov x,al

mov ah,1
int 21h
mov y,al

mov ah,1
int 21h
mov z,al


mov dl,0Ah
mov ah,2
int 21h

mov dl,0dh
mov ah,2
int 21h  

lea dx,msg6
mov ah,9
int 21h

lea dx,msg6
mov ah,9
int 21h

lea dx,msg6
mov ah,9
int 21h

lea dx,msg6
mov ah,9
int 21h

lea dx,msg6
mov ah,9
int 21h


lea dx,msg3
mov ah,9
int 21h

lea dx,msg6
mov ah,9
int 21h

lea dx,msg6
mov ah,9
int 21h

lea dx,msg6
mov ah,9
int 21h

lea dx,msg6
mov ah,9
int 21h

lea dx,msg6
mov ah,9
int 21h



ret