
; You may customize this and other start-up templates; 
; The location of this template is c:\emu8086\inc\0_com_template.txt

org 100h

.data
msg db "BX is lerger $"

.code
mov ax, @data
mov ds,ax


mov ax,5000
mov bx,500
mov cx,50


cmp ax,bx
jl axif
jg axelse

axif: cmp bx,cx
      jl bxif
      jg bxelse
      
bxif: mov ax,1
      jmp end_if

bxelse: lea dx,msg
        mov ah,9
        int 21h
        jmp end_if 
        
axelse : mov cx,1
        
end_if:

ret




