
; You may customize this and other start-up templates; 
; The location of this template is c:\emu8086\inc\0_com_template.txt

org 100h

.data
msg db "Give a input:  $" 
char db ?
.code

mov ax, @data
mov ds,ax

lea dx,msg
mov ah,9
int 21h
       

mov ah, 1
int 21h

sub al,20h
mov char,al

mov dl,0ah
mov ah,2
int 21h

mov dl,0dh
mov ah,2
int 21h

mov dl,char
mov ah,2
int 21h

ret