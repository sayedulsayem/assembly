
; You may customize this and other start-up templates; 
; The location of this template is c:\emu8086\inc\0_com_template.txt

org 100h

.data
msg db "this is a test msg $"
x db ?
y db ?
z db ?


.code
mov ax,@data
mov ds,ax

lea dx,msg
mov ah,9
int 21h

mov ah,1
int 21h
mov x,al

mov ah,1
int 21h

mov ah,1
int 21h
mov y,al

mov ah,1
int 21h

mov ah,1
int 21h
mov z,al

mov ah,1
int 21h

mov dl,0Ah
mov ah,2
int 21h

mov dl,0dh
mov ah,2
int 21h

mov dl,x
mov ah,2
int 21h

mov dl,0Ah
mov ah,2
int 21h

mov dl,0dh
mov ah,2
int 21h

mov dl,y
mov ah,2
int 21h


mov dl,0Ah
mov ah,2
int 21h

mov dl,0dh
mov ah,2
int 21h

mov dl,z
mov ah,2
int 21h

ret




