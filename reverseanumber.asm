
; You may customize this and other start-up templates; 
; The location of this template is c:\emu8086\inc\0_com_template.txt

org 100h

mov al,11011100b

mov cx,8

reverse: SHL al,1
         RCR bl,1
         loop reverse
        
         mov al,bl
         
         mov dl,al
         mov ah,2
         int 21h
        
        


ret




   