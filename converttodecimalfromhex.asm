
; You may customize this and other start-up templates; 
; The location of this template is c:\emu8086\inc\0_com_template.txt

org 100h

.data
msg db "Enter a hex. digit : $"
msg2 db "In decimal it is : $"
x db ?
y db ?

.code
mov ax,@data
mov ds,ax

lea dx,msg
mov ah,9
int 21h

mov ah,1
int 21h
mov x,al

mov dl,0Ah
mov ah,2
int 21h

mov dl,0Dh
mov ah,2
int 21h

mov al, "1"
mov y, al

mov al,x
sub al,11h
mov x,al

lea dx,msg2
mov ah,9
int 21h

mov dl,y
mov ah,2
int 21h

mov dl,x
mov ah,2
int 21h



ret




