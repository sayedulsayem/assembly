
; You may customize this and other start-up templates; 
; The location of this template is c:\emu8086\inc\0_com_template.txt

org 100h

.data
x db ?
cngx db ?
msg db "This is a hexadecimal digit $"


.code
mov ax,@data
mov ds,ax

mov ah,1
int 21h
mov x,al

mov dl,0ah
mov ah,2
int 21h
        
mov dl,0dh
mov ah,2
int 21h


mov bl,x

         
cmp bl,"Z"
jle smallerfrombigz
jg notcapitalletter


smallerfrombigz:
                 cmp bl,"A"
                 jge thisiscapitalletter
                 
notcapitalletter: cmp bl,"a"
                  jge biggerfromsma
              
thisiscapitalletter: add bl,20h
                     mov cngx,bl
                     
                     mov dl,cngx
                     mov ah,2
                     int 21h
                     jmp endif
                     
                      
biggerfromsma: cmp bl,"z"
               jle thisissmletter
               
thisissmletter: sub bl,20h
                mov cngx,bl
                
                
                mov dl,cngx
                mov ah,2
                int 21h
                
                mov cngx,al
                
                cmp al,"F"
                jle hextest
                jg endif 
                
                
hextest: cmp al,"A"
         jge  thisishexadeciaml
                
                
thisishexadeciaml: mov dl,0ah
                   mov ah,2
                   int 21h
        
                   mov dl,0dh
                   mov ah,2
                   int 21h

                   lea dx,msg
                   mov ah,9
                   int 21h

                
                

endif:

ret




