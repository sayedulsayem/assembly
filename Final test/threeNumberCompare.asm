
; You may customize this and other start-up templates; 
; The location of this template is c:\emu8086\inc\0_com_template.txt

org 100h 

.data
msgaxlg db "AX contains LARGEST value $"
msgaxsm db "AX contains SMALLEST value $"
msgbxlg db "BX contains LARGEST value $"
msgbxsm db "BX contains SMALLEST value $"

.code
mov ax,@data
mov ds,ax


mov ax,345
mov bx,435
mov cx,543

cmp ax,bx
jg axgbx
jl axlbx

axgbx: cmp bx,cx
       jg axlg
       jl bxsm
       
axlbx: cmp bx,cx
       jg bxlg
       jl axsm
       
axlg:lea dx,msgaxlg
     mov ah,9
     int 21h
     jmp endif

axsm:lea dx,msgaxsm
     mov ah,9
     int 21h
     jmp endif
     
bxlg:lea dx,msgbxlg
     mov ah,9
     int 21h
     jmp endif
     
bxsm:lea dx,msgbxsm
     mov ah,9
     int 21h
     jmp endif


endif:

ret




